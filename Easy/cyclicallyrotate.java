/*https://practice.geeksforgeeks.org/problems/cyclically-rotate-an-array-by-one2614/1/?category[]=Arrays&category[]=Arrays&difficulty[]=-1&page=1&query=category[]Arraysdifficulty[]-1page1category[]Arrays#
 
We have many approaches to solve this problem. Let's see one by one what they are:

Approach One

This is very basic approach that one could think of:
   1. store the last element in temp variable. 
   2. shift all elements right side.
   3. replace the first element with temp (i.e. last variable)
   
Approach Two

Similar, to approach one. Instead of storing the last element we can store first element and 
then left shift all the elements and then replace last element with first stored in temp variable.

What is the problem with above approaches?
They will work perfectly fine when d=1 with time complexity of O(n) and space complexity of O(1).
What if d i.e rotating factor is not equal to 1? Then we have to call this function d times with increase in TC of O(n*d). 
-- Below is Approach 3 steps.
    1. call rotatebyone function from above approaches for 0 to d-1 times. 
    
Approach 4
As in previous case, we are compromising on Time complexity. How can we optimize it?
  1. store first d elements in temp [].
  2. shift all the elements to left side. 
  3. store back the temp [] in original array.
  
  What did you realize? Time complexity is now O(n) but space complexity is now increased to O(d)
  
Approach 5
Reversal Algorithm(Reverse method)
   rotate(arr[], d, n)
    reverse(arr[], 1, d) ;
    reverse(arr[], d + 1, n);
    reverse(arr[], 1, n);
     Time complexity of O(n)

-- All implementations can be found here - https://www.geeksforgeeks.org/array-rotation/
                                           https://www.geeksforgeeks.org/program-for-array-rotation-continued-reversal-algorithm/

*/
     
  
class Compute {
    
    public void rotate(int arr[], int n)
    {
        //My solution 
      int i;
      int temp = arr[0];
      arr[0] = arr[n-1];
    
      for(i=1;i<n;i++)
            {
              arr[n-1] = temp;
              temp = arr[i];
              arr[i]=arr[n-1];
            }
            
            //   int x = arr[n - 1], i;
            //   for (i = n - 1; i > 0; i--)
            //     arr[i] = arr[i - 1];
            //     arr[0] = x;
    
}