/* https://www.interviewbit.com/old/problems/balance-array/

-- Aproach with Time and space complexity O(n) and O(1) respectively.

If we have to check for magic element then
sum of odd indexes = sum of even indexes
so,
leftoddsum + rightevensum == leftevensum + rightoddsum

You may wonder what is this weird statement and how it will give magic element check?
When you remove any ith element from array, left side of odd and even indexes remain same but right side even indexes become odd and vice versa.
so, leftoddsum + rightevensum – this gives sum of all odd indexes as rightevensum will give oddsum if removed i element.
and same for even sum.

Consider an array of size 5,
Now let’s say we are at 3rd index and checking if 3rd element is magic element or not.
when we are at 3rd index,
left side of 3rd index we have (0 1 2 indexes)
so leftoddsum = a[1]
leftevensum = a[0]+a[2]
right side of 3rd index we have only 4th index
so rightoddsum = 0
rightevensum = a[4]
we will check with above equation and count++ if true

Now let’s move to 4th index,
What will change?
leftodd sum will change as now we have 1 and 3 index so we will add a[3] to left odd sum.
righteven sum will also change as now we have to remove a[4] from the even sum and it will be 0.

So, every time when we have ith index as even we have to update leftoddsum and rightevensum
and when i index is odd we have to update lefteven and rightodd.

Pusedocode

Initialize leftOddSum,leftEvenSum,count = 0.
calculate initial rightOddSum and rightEvenSum by calculating odd and even sum of entire array.
Run loop on array (i -> 0 to n-1)
a. if even
—>a. if 0th element
---------->updateRightEven(a[i]) //For first element we only have right side and even will only be updated as 0 is even index. RightOddSum wil be same.
---->else not 0th element
--------->updateLeftOdd(a[i-1])
----------->updateRightEven(a[i])
else odd
------>updateLeftEven(a[i-1])
----->updateRightOdd(a[i])
b. Check for magic element if true then count++.
repeat this for all elements in array.
Return count.
updateRightEven – rightEvenSum-a[i]
updateRightOdd – rightOddSum-a[i]
updateLeftEven – leftEvenSum-a[i-1]
updateLeftOdd – leftOddSum-a[i-1]

We don’t need separate if for covering edge cases like n=1 and n=2
— n=1, magic element count can be covered with above approach will always be 1.
— n=2, magic element count can be covered with above approach and will always be 0.

*/
public class Solution {
    
     int leftOddSum,leftEvenSum,count = 0;
    int rightOddSum,rightEvenSum;
    
    public int solve(ArrayList<Integer> A) {
       
    int n = A.size();
    
    for(int i=0;i<n;i++)
    {
        if(i%2==0)
            rightEvenSum+=A.get(i);
        else
            rightOddSum+=A.get(i);
    }

    for(int i=0;i<n;i++)
    {
        if(i%2==0)
        {
            if(i==0)
                updateRightEven(A.get(i));
            else
            {
                updateLeftOdd(A.get(i-1));
                updateRightEven(A.get(i));
            }       
        }
        else
        {
            updateLeftEven(A.get(i-1));
            updateRightOdd(A.get(i));
        }    
    if(leftOddSum+rightEvenSum == leftEvenSum+rightOddSum)
        count++;
    }

    return count++;
    
    }
    public void updateRightEven(int x)
        {
          rightEvenSum-= x;
        }
    
    public void updateLeftEven(int x)
        {
          leftEvenSum+= x;
        }
        
    public void updateRightOdd(int x)
        {
          rightOddSum-= x;
        }
        
    public void updateLeftOdd(int x)
        {
          leftOddSum+= x;
        }
}
