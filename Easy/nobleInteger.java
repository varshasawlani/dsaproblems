/* https://www.interviewbit.com/problems/noble-integer/

Brute force approach with TC of O(n2)
    Iterate over array and check if that element is greater then a[i] element if yes then increase the count. 
    Check if count is equal to a[i]. if yes then return 1.
    else return -1.
    
 int count;
        int n = A.length;

        for(int i =0;i<n;i++)
        {
            count=0;
            for(int j=0;j<n;j++)
                if(A[j]>A[i])
                    count ++;
            if(A[i]==count)
                return 1;
        }
                    
    return -1;
    
Efficient Approach with TC O(nLog(n))
    If array is sorted then our solution will be easy.
    we don't need to check if element is greater then a[i] it will always be greater then a[i].
    we just need to check remaining elements are equal to a[i]
        i.e size of array -1 -i == a[i] //return 1
    edge cases to handle here is if elements are duplicate 
        eg:- 1 2 2 3
            here we will get 1 but correct answer will be -1. So, to handle this we need to go to the last occurence of any element.
            that can be checked by comparing ith and i+1th element. 
            Once we reach the last occurence we are good to go with noble integer check. 
            
   NOTE:: We don't need to cover negative integer cases. Answer will always be -1 in that case.
            
*/

public class Solution {
    public int solve(ArrayList<Integer> A) {

        int n = A.size();
        //Sort the array 
        Collections.sort(A);

        for(int i = 0; i<n;i++)
            {
                //Handling duplicate case
                if(i<n-1 && A.get(i) == A.get(i+1))
                    continue;
                
                //Checking for noble integer
                if(n-1-i==A.get(i))
                    return 1;
            }
        return -1;


        
    }
        
}


    