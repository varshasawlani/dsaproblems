/*Refer problem statement here - https://www.hackerearth.com/practice/data-structures/arrays/1-d/practice-problems/algorithm/maximize-sum-0423b95e/

First Approach 
We will sort the array and select max k elements from the array. 
This approach is not correct as we don't have to select first k integers but k distinct integers. So, for example a = {2,2,2,4,5}
Our algo will give output 5+4 =9 which is incorrect as it is not maximun. Maximun sum is 2+2+2+5 = 11.

Second Approach
Instaed of choosing max k we have to choose max k element*occurrence (i.e 2*3 = 6 , 5*1 = 5, 4*1 = 4 --> we choose 6 and 5. And, 5,2 as k distinct integers.
-- only positive integers are covered in this approach. 
*/

import java.util.*;

class TestClass {
    public static void main(String args[] ) throws Exception {
       
        Scanner s = new Scanner(System.in);
        int t = s.nextInt();
        while(t-->0)
        {
            
            int n = s.nextInt(); 
            int k = s.nextInt();
            HashMap<Integer,Long> map = new HashMap<Integer,Long>();
            
            int x=0;
            long sum = 0;

    /* Reading all the input elements and storing it in form of element,occurrence. Hashmap size will not be equal to n elements as repeated elements will get replaced
     i.e repeated key -- it's value will be updated 
    for example in our test case 1st Iteration- 2,2 2nd Iteration --> 2,4. Final hashmap will be [(2,4),(1,1),(5,5)] */
    
            for(int i=0;i<n;i++)
                {
                    int element = s.nextInt();
                    map.put(element,map.getOrDefault(element,(long)0)+(long)element);
                }
            
            //decalaring an array with size of hashmap size to optimize space 
            long [] a = new long[map.size()];

    //Fetching all the occurrences and storing it in array 
            for(Map.Entry<Integer,Long> m: map.entrySet())
                a[x++] = m.getValue();

            Arrays.sort(a);
    

    /* Array is sorted in ascending order. Hence, fetching last k elements from sorted array. 
    Here, we have additional condition to check j<m because when k is more than size of array we will get array index out of bound exception */
    
            int m = a.length;
            for(int j=0; j<k && j<m; j++)
                {
                    if(a[m-1-j]>0)
                        sum = sum + a[m-1-j];
                    else 
                        break;
                }
            System.out.println(sum);
               
        }
       
    }
}
