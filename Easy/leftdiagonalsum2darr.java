
//Problem Statement - https://practice.geeksforgeeks.org/problems/java-1-d-and-2-d-array2952/1/?category[]=Arrays&category[]=Arrays&difficulty[]=-1&page=1&query=category[]Arraysdifficulty[]-1page1category[]Arrays

//Basic Approach - left dagonal elements will be 00 11 22 and ... so on. So, sum will only be updated if i==j.

class Complete
{
    public static ArrayList<Integer> array(int a[][], int b[], int n)
    {
        // Complete the function
        int max = b[0]; //Initializing the max with first element in an array.
        int sum = 0;
        
      //Sum of left diagonal of 2d array
        for(int i =0;i<n;i++)
         {   
            for(int j = 0;j<n;j++)
                {   
                if(i==j)
                    sum = sum + a[i][j];
                }
         }
         
         
         //Max of 1 d array
         for(int k =0;k<n;k++)
            {
                if(b[k]>max)
                    max = b[k];
            }
            
            ArrayList<Integer> arrayList =new ArrayList<Integer>();
            arrayList.add(sum);
            arrayList.add(max);
            
            return arrayList;
                    
    }
}