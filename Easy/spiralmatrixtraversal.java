//https://practice.geeksforgeeks.org/problems/spirally-traversing-a-matrix-1587115621/1

// { Driver Code Starts
import java.io.*;
import java.util.*;
class GFG
{
    public static void main(String args[])throws IOException
    {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        
        while(t-- > 0)
        {
            int r = sc.nextInt();
            int c = sc.nextInt();
            
            int matrix[][] = new int[r][c];
            
            for(int i = 0; i < r; i++)
            {
                for(int j = 0; j < c; j++)
                 matrix[i][j] = sc.nextInt();
            }
            Solution ob = new Solution();
            ArrayList<Integer> ans = ob.spirallyTraverse(matrix, r, c);
            for (Integer val: ans) 
                System.out.print(val+" "); 
            System.out.println();
        }
    }
}// } Driver Code Ends


class Solution
{
    //Function to return a list of integers denoting spiral traversal of matrix.
    static ArrayList<Integer> spirallyTraverse(int matrix[][], int r, int c)
    {
     
        int T = 0;   //Top most Pointer for row(0th row)
        int B = r-1; //Bottom most Pointer for ro(m-1th row)
        int L = 0;   //Left most pointer for column (0th column)
        int R = c-1; //Right most pointer for column (n-1th column)
        int d = 1; // Directions

        ArrayList<Integer> outputArray = new ArrayList<Integer>();

        while(T<=B && L<=R)
        {
            //left to right
            for(int i=L; i<=R; i++)
                outputArray.add(matrix[T][i]);
                    
            T++;
            
             //Top to Bottom
            for(int i=T; i<=B; i++)
                outputArray.add(matrix[i][R]);   
                      
                        
            R--;
            
             //Right to Left
            if(T<=B)
            {
            for(int i=R;i>=L;i--)
                outputArray.add(matrix[B][i]);
                    
            B--;
            }
            
            //Bottom to Top
            if(L<=R)
            {
            for(int i=B;i>=T;i--)
                outputArray.add(matrix[i][L]);
                      
            L++;
            }
            
     }
    
    return outputArray;
    }
}

