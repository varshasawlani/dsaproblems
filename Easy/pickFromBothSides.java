//https://www.interviewbit.com/problems/pick-from-both-sides/
/* Initial approach which did not work
--- Greedy approach just keep on picking maximum element from both sides till b. 
This will fail when we an array like this -- 5,100,2,50 and b = 2 
You will pick 50 first and then you will pick 5 sum will be 55 but you should have picked 5 and 100 then sum should be 105

Efficient and correct approach will be,
1. To compare all the combinations of b elements for example if b is 3 then possible combinations are:

    1 from left + 2 from right 
    2 from left + 1 from right 
    3 from left + 0 from right
    same for right side as well
    
We just need to cover all combinations and check for maximum sum. 
Calculate the sum of first b elements from left and in every iteration of b remove one element from left and add one from right and
update the max sum accordingly */

public class Solution {
    public int solve(ArrayList<Integer> A, int B) {
        
    
        int sum = 0,R=A.size()-1,L=B,j=B;
    
        //Compute the sum of first b elements from the left side
        for(int i=0;i<B;i++)
        {
            sum = sum + A.get(i);
        }
        
        int maxSum = sum;
        //Now in every iteration of B
        while(j>0)
        {
         //Remove B-1 element from left
         sum = sum - A.get(--L);
         //Add n-1 element from right 
         sum = sum + A.get(R--);
         //Compare sum and maxSum and store maximum in MaxSum
         if(sum>maxSum)
            maxSum = sum;
        j--;
        }
    return maxSum;
    }
}
